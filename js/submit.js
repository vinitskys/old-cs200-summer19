let generateSubmissionPage = function(submitSpan){

	let workbookID = localStorage.getItem("workbookID");

	if (getValue(workbookID + "_completed")){
		submitSpan.innerHTML = "<iframe src=\"https://docs.google.com/forms/d/e/1FAIpQLSe-3j3ZYufNn6CpA_YMa8ICyfzdKV4TOw1qcETSlxP4W07myw/viewform?embedded=true\" width=100% height=100% frameborder=\"0\" marginheight=\"0\" marginwidth=\"0\">Loading...</iframe>";	
		return;
	}

	newHTML = ""

	// use _checked for q's


	newHTML += "<p> <b>";
	newHTML += "Once you submit this workbook, you will be unable to make any changes. ";
	newHTML += "You will be graded based on completion as well as accurary. ";
	newHTML += "Please check to make sure you have completed EVERY page for the workbook before submitting. ";
	
	newHTML += "<br><br>";

	newHTML += "After submitting, please complete the feedback form that will appear. ";
	newHTML += "I will not use your answers to the feedback survey as part of calculating your grade, ";
	newHTML += "but you will receive points for completing the form. ";

	newHTML += "</b> </p>";

	newHTML += "<button class=\"button\" type=\"button\" id=\"workbookSubmitButton\"> Submit Workbook </button>";

	submitSpan.innerHTML = newHTML;

	let submitButton = document.getElementById("workbookSubmitButton");
	submitButton.onclick = function(){
		if (confirm("Are you sure you want to submit this workbook? You will no longer be able to make any changes.")){
			saveValue(workbookID + "_completed", true);			

			let userID = localStorage.getItem("userID");

			$.ajax({
		       type: "GET",
		       url: "../../../scripts/submitWorkbook.php",
		       data: {
		       		userID: userID,
		       		workbookID: workbookID,
		       },
		       success: function(result){
		       		alert("Workbooks submitted successfully!");
		       		submitSpan.innerHTML = "<iframe src=\"https://docs.google.com/forms/d/e/1FAIpQLSe-3j3ZYufNn6CpA_YMa8ICyfzdKV4TOw1qcETSlxP4W07myw/viewform?embedded=true\" width=100% height=100% frameborder=\"0\" marginheight=\"0\" marginwidth=\"0\">Loading...</iframe>";	
				},
		     	
		     	error: function(){
					alert("Something went wrong. Please email Sam so he can look into it.");
		     	}
			});

		}
	}

	
}