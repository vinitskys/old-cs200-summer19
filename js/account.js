let login = function(userID){

	let workbookID = localStorage.getItem("workbookID");

	$.ajax({
    	type:    "GET",
    	url:     "../scripts/login.php",
        async: false,
        data: {
       		userID: userID,
       		workbookID: workbookID,
		       },
    	success: function(result) {
            
            let obj = JSON.parse(result);
            let keys = Object.keys(obj);
            for (i in keys){
                key = keys[i];
                localStorage.setItem(key, obj[key]);
            }

            localStorage.setItem("userID", userID);
    	},
    	error:   function() {
        	alert("Error");
    	}
    });

    localStorage.setItem("userID", userID);

}

let logout = function(){
	if (confirm("Are you sure you want to log out?")){
		localStorage.clear();
		window.location = "../../../pages/logout.html";
	}
}
