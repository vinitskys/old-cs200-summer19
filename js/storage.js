let saveValue = function(item, value){
	let userID = localStorage.getItem("userID");
	let workbookID = localStorage.getItem("workbookID");

	localStorage.setItem(item,value);

	let jsonLocalStorage = JSON.stringify(localStorage);

	$.ajax({
       type: "GET",
       url: "../../../scripts/saveValue.php",
       data: {
       		userID: userID,
       		workbookID: workbookID,
       		storage:jsonLocalStorage,
       },
       success: function(result){
		},
     	
     	error: function(){
			alert("Something went wrong. Please email Sam about what you tried doing, so he can look into it.");
     	}
	});
}

let getValue = function(item){

	let result = localStorage.getItem(item);

	return result;
}