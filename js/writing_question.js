let submitWritingPrompt = function(content,questionID){

	let userID = localStorage.getItem("userID");
	let workbookID = localStorage.getItem("workbookID");
	let pageID = localStorage.getItem("pageID");

	$.ajax({
       type: "GET",
       url: "../../../scripts/submitWritingPrompt.php",
       data: {
       		content:content,
       		pageID: pageID,
       		problemID: questionID,
       		userID: userID,
       		workbookID: workbookID
       },
       
       success: function(result){
       		alert("Thank you for submitting. You may resubmit this exercise at any time.");
       },
     	
     	error: function(){
			alert("Uh oh, something went wrong. Please email Sam, and include which workbook this is, your netID, and what you just tried submitting.");
     	}
	});
}

let generateWritingPrompt = function(prompt, questionID){
	
	let userID = localStorage.getItem("userID");
	let workbookID = localStorage.getItem("workbookID");
	let pageID = localStorage.getItem("pageID");

	let uniqueID = workbookID + "_" + pageID + "_" + questionID;

	newHTML = "";

	newHTML += "<div id=\"writingBox\"style=\"border:1px solid black; padding:10px; margin-bottom: 20px\">"
	newHTML += "<b>" + prompt + "</b>";
	newHTML += "<div style=\"border-top:1px dashed black; padding-top:10px;\">";
	newHTML += "<div style=\"margin-bottom:20px\">";

	newHTML += "<textarea id=\"textInput\" style=\"width:100%; height:100px\"></textarea>"
	newHTML += "</div>";
	newHTML += "<input type=\"button\" class=\"button\" id=\"" + uniqueID + "_submit_button\" value=\"Submit\" style=\"color:black\">";
	newHTML += "</div> </div>";
	newHTML += "<div id=\"sub\" hidden> You have already submitted this once. You may resubmit it, and your most recent submission will be graded.</div>";

	document.getElementById(questionID).innerHTML = newHTML;
	document.getElementById("sub").hidden=true;

	let submitButton = document.getElementById(uniqueID + "_submit_button");
	let inputElement = document.getElementById("textInput");

	inputElement.value = getValue(uniqueID);

	inputElement.oninput = function(){
		saveValue(uniqueID, inputElement.value);
	}

	submitButton.onclick = function(){
		submitWritingPrompt(inputElement.value, questionID);
		document.getElementById("sub").hidden=false;
	}

	if (getValue(uniqueID)!=null){
		document.getElementById("sub").hidden=false;
	}

}