let correctColor = "#0e0";
let incorrectColor = "#e00";

let submitProblem = function(questionID, answer){

	let userID = localStorage.getItem("userID");
	let workbookID = localStorage.getItem("workbookID");
	let pageID = localStorage.getItem("pageID");

	$.ajax({
       type: "GET",
       url: "../../../scripts/submitProblem.php",
       data: {
       		pageID: pageID,
       		problemID: questionID,
       		userID: userID,
       		workbookID: workbookID,
       		answer: answer
       },
       
       success: function(result){
       },
     	
     	error: function(){
			alert("Uh oh, something went wrong. Please email Sam, and include which workbook this is, your netID, and what you just tried submitting.");
     	}
	});
}

let recordAnswer = function(questionID, answer){

	let workbookID = localStorage.getItem("workbookID");
	let pageID = localStorage.getItem("pageID");

	$.ajax({
       type: "GET",
       url: "../../../scripts/recordAnswer.php",
       data: {
       		pageID: pageID,
       		problemID: questionID,
       		workbookID: workbookID,
       		answer: answer
       },
       
       success: function(result){
       },
     	
     	error: function(){
			alert("Uh oh, something went wrong. Please email Sam, and include which workbook this is, your netID, and what you just tried submitting.");
     	}
	});
}

let generateQuizQuestion = function(prompt, answers, explanations, correctAnswerIndex, questionID){
	
	let workbookID = localStorage.getItem("workbookID");
	let pageID = localStorage.getItem("pageID");

	let uniqueID = workbookID + "_" + pageID + "_" + questionID
	
	newHTML = "";

	newHTML += "<div style=\"border:1px solid black; padding:10px; margin-bottom: 20px\">"
	newHTML += "<b>" + prompt + "</b>";
	newHTML += "<div style=\"border-top:1px dashed black; padding-top:10px;\">";
	newHTML += "<div style=\"margin-bottom:20px\">";

	for (i in answers){
	
		let answer = answers[i];

		newHTML += "<div id=" + questionID + "_div_" + i + ">";
		
		// input
		newHTML +=	"<input type=\"radio\" name=" + questionID ;
		newHTML += " id=" + questionID + "_radio_answer_" + i +  " value=" + i;
		newHTML += " style=\"display:inline-block; margin-right:5px\"";
		newHTML += ">";

		newHTML += "<label id=\""+ questionID + "_label_" + i +"\"";
		newHTML += "style=\"display:inline-block; padding:0px 5px\" for=" + i + ">" + answer + "</label>";
		newHTML += "<div id=\"" + questionID + "_exp_" + i + "\" style=\"display:inline-block; padding:0px 10px\"></div>";
		newHTML += "</div>";
	}
	
	newHTML += "</div>";
	newHTML += "<input type=\"button\" class=\"button\" id=\"" + questionID + "_submit_button\" value=\"Submit\" style=\"color:black\">";
	newHTML += "</div> </div>";

	let questionSpan = document.getElementById(questionID);
	questionSpan.innerHTML = newHTML;

	for (i in answers){
		let answerInputElement = document.getElementById(questionID + "_radio_answer_" + i);
		answerInputElement.checked = (i == getValue(uniqueID + "_"  + "answerIndex") ? true : false);
		answerInputElement.disabled = getValue(uniqueID + "_" + "correct");

		let answerLabelElement = document.getElementById(questionID + "_label_" + i);
		let bg = getValue(uniqueID + "_bg_" + i);
		answerLabelElement.style.background = bg;
		
		let answerExpElement = null;
		if (getValue(uniqueID + "_correct")){
			for (j in answers){
				answerExpElement = document.getElementById(questionID + "_exp_" + j);
				answerExpElement.innerHTML = explanations[j];
			}
		}

		if (getValue(uniqueID + "_guessed_" + i)){
			answerExpElement = document.getElementById(questionID + "_exp_" + i);
			answerExpElement.innerHTML = explanations[i];
			answerInputElement.disabled = true;
		} 
		
		answerInputElement.onclick = function(){
			saveValue(uniqueID + "_answerIndex", this.value);

			for (j in answers){
				let otherAnswerInputElement = document.getElementById(questionID + "_radio_answer_" + j);
				let otherAnswerLabelElement = document.getElementById(questionID + "_label_" + j);

				if (getValue(workbookID + "_" + pageID +"_" + questionID +"_guessed_" + j)){
						otherAnswerLabelElement.style.background = incorrectColor;
				} else if (otherAnswerInputElement.checked){
					otherAnswerLabelElement.style.background = "lightgray";
				} else {
					otherAnswerLabelElement.style.background = "";
				}
				
				saveValue(uniqueID + "_bg_" + j, otherAnswerLabelElement.style.background);
			}
		}

		answerLabelElement.onclick = function(){

			if (document.getElementById(questionID + "_radio_answer_" + $(this).attr('for')).disabled){
				return;
			}

			if (getValue(uniqueID + "_correct")){
				return;
			}

			for (i in answers){
				let otherInputElement = document.getElementById(questionID + "_radio_answer_" + i);
				otherInputElement.checked=false;
			}

			let inputElement = document.getElementById(questionID + "_radio_answer_" + $(this).attr('for'));
			inputElement.checked=true;
			inputElement.onclick();
		}

	}

	let submitButton = document.getElementById(questionID + "_submit_button");

	submitButton.disabled = getValue(uniqueID + "_" + "correct");

	submitButton.onclick = function(){


        let answerIndex = getValue(uniqueID + "_answerIndex");

		if (answerIndex === null){
			return;
		}

        if (getValue(uniqueID + "_answered") != "saved"){
            recordAnswer(uniqueID, answerIndex);
            saveValue(uniqueID + "_answered", "saved");
        }
        
		let answerInputElement = document.getElementById(questionID + "_radio_answer_" + answerIndex);
		let answerLabelElement = document.getElementById(questionID + "_label_" + answerIndex);
		let answerExpElement = document.getElementById(questionID + "_exp_" + answerIndex);

		answerExpElement.innerText = explanations[answerIndex];

		// get it wrong? then give them another changed
		if (answerIndex != correctAnswerIndex){
			answerLabelElement.style.background = incorrectColor;
			saveValue(uniqueID + "_bg_" + answerIndex, answerLabelElement.style.background);

			saveValue(workbookID + "_" + pageID +"_" + questionID +"_guessed_" + answerIndex, true);
			document.getElementById(questionID + "_radio_answer_" + answerIndex).disabled = true;
			submitProblem(questionID, answerIndex);
		} else {

			submitProblem(questionID, answerIndex);

			answerLabelElement.style.background = correctColor;

			saveValue(uniqueID + "_bg_" + answerIndex, answerLabelElement.style.background);
			saveValue(uniqueID + "_" + "correct", true);
			this.disabled = true; // submit button

			for (i in answers){ // radio buttons
				document.getElementById(questionID + "_radio_answer_" + i).disabled = true;
				document.getElementById(questionID + "_label_" + i).disabled = true;
				
				let answerExpElement = document.getElementById(questionID + "_exp_" + i);
				answerExpElement.innerHTML = explanations[i];
			}
		}
	}

}
