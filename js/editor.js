let loadEditor = function(editorID, wrapperTypesArray, options={}){

		let userID = localStorage.getItem("userID");
		let workbookID = localStorage.getItem("workbookID");
		let pageID = localStorage.getItem("pageID");	

		let uniqueID = workbookID + "_" + pageID + "_" + editorID;	

		let hideTypeHeader = false || options.hideTypeHeader;
		let isSandbox = false || options.isSandbox;
		let hideOutputBox = options.hideOutputBox || false;

		//<button type=\"button\" id=\"save"+ uniqueID +"\" class=\"button\">Save</button>\

		let editorHTML = "\
		<div class=\"full_editor\" id=\"full_editor"+ uniqueID +"\">\
		<div class=\"typeInfoBox\" id=\"typeInfoBox"+ uniqueID +"\"> <span class=\"typeInfo\" id=\"typeInfo"+ uniqueID +"\"></span></div>\
		<div class=\"editor_toolbar_combo\" id=\"editor_toolbar_combo"+ uniqueID +"\">\
			<div class=\"toolbar\" id=\"toolbar"+uniqueID+"\">\
					<button type=\"button\" id=\"undo"+ uniqueID +"\" class=\"button\">Undo</button>\
					<button type=\"button\" id=\"redo"+ uniqueID +"\" class=\"button\">Redo</button>\
					<button type=\"button\" id=\"down"+ uniqueID +"\" class=\"button down\">Download a Copy</button>\
					<button type=\"button\" id=\"reset"+ uniqueID +"\" class=\"button reset\">Restore to default</button>\
				</div>\
			<pre class=\"editor\" id=\"editor"+ uniqueID +"\"></pre>\
		</div>\
		</div>\
		<div class=\"terminal\" id=\"terminal"+ uniqueID +"\">\
			Input: <input type=\"text\" class=\"terminalIO\" id=\"terminalInput"+ uniqueID +"\" autocomplete=\"off\" autocorrect=\"off\" autocapitalize=\"off\" spellcheck=\"false\"> \
			 <span id=\"programOutputSpan"+ uniqueID +"\">Output: <input type=\"text\" class=\"terminalIO\" id=\"terminalOutput"+ uniqueID +"\" disabled></span>\
			<br>\
			<button type=\"button\" id=\"compileButton"+ uniqueID +"\" class=\"button compileButton\">Compile</button>\
			<button type=\"button\" id=\"runButton"+ uniqueID +"\" class=\"button runButton\">Run</button>\
			<div class= \"wrapper\"></div>\
				<pre id=\"compilerWindow"+ uniqueID +"\" class=\"window\"><b>Compiler Log: </b><p id=compilerOutput"+ uniqueID +" class=\"output\"></p></pre>\
				<pre id=\"runningWindow"+ uniqueID +"\" class=\"window\"><b>Runtime Log: </b><p id=runningOutput"+ uniqueID +" class=\"output\"></p></pre>\
			</div>\
		</div>\
		";

		document.getElementById(editorID).innerHTML = editorHTML;

		let inputNames = [];
		let inputTypes = [];
		let outputType = "";
		if (wrapperTypesArray.length == 2){
			outputType = wrapperTypesArray[wrapperTypesArray.length-1];
			inputTypes[0] = wrapperTypesArray[0];
			inputNames[0] = "input";
		} else if (wrapperTypesArray.length > 2){
			outputType = wrapperTypesArray[wrapperTypesArray.length-1];
			for (let i = 0; i < wrapperTypesArray.length-1; i++){
				inputTypes[i] = wrapperTypesArray[i];
				inputNames[i] = "input" + (i+1);
			}
		}

		if (!hideTypeHeader){
			let typeText = "You have the following input variables: <ul class=\"varList\">";
			for(let i = 0; i < inputNames.length; i++){
				typeText += "<li class=\"varListItem\"><span style=\"font-family: courier new\"><b>"+ inputNames[i] + "</b></span>, whose data type is <b>" + inputTypes[i] +"</b> </li>";
			}
			
			typeText += "</ul> <hr style=\"margin: 5px 10px 5px 0px\">The output variable is: <ul class=\"varList\"><li class=\"varListItem\"><span style=\"font-family: courier new\"><b> output</b></span>, whose data type is <b>"+ outputType +"</b></li></ul>";
			document.getElementById("typeInfo" + uniqueID).innerHTML = typeText;
		} else {
			let typeInfoBox = document.getElementById("typeInfoBox"+ uniqueID);
			typeInfoBox.style.display = "none";
		}

		if (hideOutputBox){
			let outputSpan = document.getElementById("programOutputSpan"+ uniqueID);
			outputSpan.style.display = "none";
		}

		var editor = ace.edit("editor" + uniqueID);

		editor.setOptions({
			theme:"ace/theme/eclipse",
			mode:"ace/mode/java",
			autoScrollEditorIntoView: true,
			minLines: 5,
			maxLines: 10,
			showFoldWidgets: false,
			wrap: true,
		});

		// let saveButton = document.getElementById("save"+ uniqueID);
		let undoButton = document.getElementById("undo"+ uniqueID);
		let redoButton = document.getElementById("redo"+ uniqueID);
		// saveButton.disabled = true;
		undoButton.disabled = true;
		redoButton.disabled = true;

		let resetButton = document.getElementById("reset"+ uniqueID);
		let downButton = document.getElementById("down"+ uniqueID);

		let compile = document.getElementById("compileButton"+ uniqueID);
		let run = document.getElementById("runButton"+ uniqueID);
		run.disabled = true;

		let terminalInput = document.getElementById("terminalInput"+ uniqueID);
		terminalInput.disabled = true;

		let terminalOutput = document.getElementById("terminalOutput"+ uniqueID);
		terminalOutput.value = "";

		terminalInput.oninput= function(){
			terminalOutput.value = "";
		}


		let compilerTerminal = document.getElementById("compilerOutput"+ uniqueID);
		let runningTerminal = document.getElementById("runningOutput"+ uniqueID);

		function updateToolbar() {
			// saveButton.disabled = editor.session.getUndoManager().isClean();
	    	undoButton.disabled = !editor.session.getUndoManager().hasUndo();
			redoButton.disabled = !editor.session.getUndoManager().hasRedo();
			
			downButton.disabled = !editor.session.getUndoManager().isClean();
			run.disabled = run.disabled || !editor.session.getUndoManager().isClean();
			terminalInput.disabled = run.disabled;
			if (terminalInput.disabled){
				// terminalInput.value="";
				// terminalOutput.value = "";
			}
			compile.disabled = false;

			save();
		}

		editor.on("input", updateToolbar);

		let defaultText = "";
		if (hideOutputBox){
			defaultText = "// Name: \npublic class Main {\n\tpublic static void main(String[] args) {\n\t\t \n \n \t} \n}";
		}	

		let ClassName = "Main";
		editor.session.setValue(getValue(uniqueID) || defaultText);
		
		// save commands
		let save = function() {
			saveValue(uniqueID, editor.getValue()); 
	        editor.session.getUndoManager().markClean();
		}

		save();

		function undo(){
			editor.undo();
		}

		function redo(){
			editor.redo();
		}

		function down(){
			download(wrap(getValue(uniqueID)), ClassName + ".java", "text/plain");
		}

		function reset(){
			let c = window.confirm("Are you sure you want to erase your code and restore to the default code?")
			if (c){
				saveValue(uniqueID, "");
				editor.setValue(defaultText);
				editor.gotoLine(1);
			}
		}

		// some nice key bindings for ya laddy
		editor.commands.addCommand({
	    	name: "save",
	    	exec: save,
			bindKey: { win: "ctrl-s", mac: "cmd-s" }
		});

		editor.commands.addCommand({
	    	name: "undo",
	    	exec: undo,
			bindKey: { win: "ctrl-z", mac: "cmd-z" }
		});

		// saveButton.onclick = save;
		undoButton.onclick = undo;
		redoButton.onclick = redo;
		downButton.onclick = down;
		resetButton.onclick = reset;

		print(compilerTerminal, "The results of compiling your program will appear here... <br>", false, "grey");
		print(runningTerminal, "The results of running your program will appear here... <br>", false, "grey");
			
		let pleaseSave = function(){
			if (!editor.session.getUndoManager().isClean()){
				return "Please save your work before leaving the page!";
			} 
		}

		window.onbeforeunload = pleaseSave;
		window.onunload = pleaseSave;

		///////////////////////////
		// COMPILE & RUN BUTTONS //
		///////////////////////////

		// make printing easy
		function clear(t){
			t.innerText = "";
		}

		function print(t, str, time=true, color=false){
			color = color ? color : "#000";

			if (time){
				t.innerHTML += "<font color=\"gray\">" + getTime() + "</font>" + ": " ;
			} 

			t.innerHTML += "<font color=\" " + color + "\">" + str + "</font>";

			t.scrollTop = t.scrollHeight;

		}

		let d = new Date();
		function getTime(){
			let h = d.getHours();
			let m = d.getMinutes();
			let s = d.getSeconds();

			let suffix = (h >= 12? "PM" : "AM");

			// not military time!
			h = (h%12==0 ? 12 : h % 12);

			// pad with 0's
			m = (m < 10 ? "0"+ m : m);
			s = (s < 10 ? "0"+ s : s);

			return h + ":" + m + ":" + s + suffix;
		}

		let getDefaultValue = function(type){
			if (type == "String"){
				return "\"\"";
			}
			if (type == "int"){
				return "0";
			}
			if (type == "double"){
				return "0.0";
			}
			if (type == "boolean"){
				return "false";
			}
		}

		let convertFromString = function(type){
			if (type == "String"){
				return "";
			}
			if (type == "int"){
				return "Integer.parseInt";
			}
			if (type == "double"){
				return "Double.parseDouble";
			}
			if (type == "boolean"){
				return "Boolean.parseBoolean";
			} 
		}

		let wrap = function(code){

			let prefix = "";
			let suffix = "";

			if (!hideOutputBox){
				prefix += "public class Main{public static void main(String[] args){";
				for (let i = 0; i < inputNames.length; i++){
					prefix += "final " + inputTypes[i] + " " + inputNames[i] + " = "+ convertFromString(inputTypes[i]) +"(args[" + i + "]);"
				}
				prefix += outputType + " output = " + getDefaultValue(outputType) + ";\n"; 
				suffix = "\nSystem.out.print(output);}}"
			}

			let out = prefix + code + suffix;
			return out;

		}

		// actually compile the code!
		// return null or error
		let compileCode = function(code){

			if (!hideOutputBox){
				for (let i=0;i<inputNames.length; i++){
					let name = inputNames[i];
					if (code.indexOf(name) < 0){
						print(document.getElementById("compilerOutput"+ uniqueID), "<b>You need to use all of the \"input\" variables in your code.</b>");
						return;
					}
				} 

				if (code.search(/output\s*=/) < 0){
					print(document.getElementById("compilerOutput"+ uniqueID), "<b>You need to set the value of the variable \"output\" in your code.</b>");
					return;
				}
			}

			code = wrap(code);

		    $.ajax({
		       type: "GET",
		       url: "../../../scripts/compile.php",
		       data: {
		       		program: code,
		       		userID: userID,
		       		workbookID: workbookID,
		       		pageID: pageID,
		       		editorID: editorID,
		       },
		       success: function(result){
					let compileMessage = "";

					document.getElementById("compileButton"+ uniqueID).disabled = true;

					if (result == ""){
						compileMessage += "Compilation successful!";
						document.getElementById("runButton"+ uniqueID).disabled = false;
						document.getElementById("terminalInput"+ uniqueID).disabled = false;
					} else {

						if (!hideOutputBox){
							result = result.replace(/.*\.java:\d+: error:/g, function(x){return "<b>Error on line " + (parseInt(x.split(":")[1])-1 )+ ":</b>"});
							result = result.replace(/cannot assign a value to final variable/g, "You cannot assign a value to the input variable");
						} else{
							result = result.replace(/.*\.java:\d+: error:/g, function(x){return "<b>Error on line " + x.split(":")[1] + ":</b>"});
						}

						if (result.indexOf("javac: file not found") == -1){
							compileMessage += "Compilation failed with error message:<br>" + result + "";
						} else {
							compileMessage += "Compilation failed due to a server error. Please email Sam about this.";
						}
						
						document.getElementById("runButton"+ uniqueID).disabled = true;
						document.getElementById("terminalInput"+ uniqueID).disabled = true;
					}

					print(document.getElementById("compilerOutput"+ uniqueID), compileMessage);
		       	},
		     	
		     	error: function(){
					print(document.getElementById("compilerOutput"+ uniqueID), "Uh oh! Something went wrong with the website. Please email Sam about this." + "<br>");         		
		     	}
			});

		}

		let runCode = function(arg){

			$.ajax({
		       type: "GET",
		       url: "../../../scripts/run.php",
		       data: {
		       		input: arg,
		       		userID: userID,
		       		workbookID: workbookID,
		       		pageID: pageID,
		       		editorID: editorID,
		       },
		       success: function(result){
					let runMessage = "";

					let outputIndex = result.indexOf("%%OUTPUT%%")
					let errorIndex = result.indexOf("%%ERRORS%%")

					let output = result.slice(outputIndex+10, errorIndex);
					let errors = result.slice(10+outputIndex + errorIndex);
					let isError = (errors.replace(/\s/g, '').length != 0);
					let isOutput = (output.replace(/\s/g, '').length != 0);

					if(!isError){
						runMessage += "Ran successfully";
					} else {
						runMessage += "Run failed";
					} 

					if(arg.length > 0){
						runMessage += " on input \'" + arg + "\'";
					}

					if(isError){
						
						if (!hideOutputBox){
							errors = errors.replace(/Main\.main\(Main\.java:\d+\)/g, function(x){return "<b>line " + (parseInt(x.split(":")[1].split(")")[0])-1) + ":</b>"});
						}

						runMessage += " with error(s): <br><b>" + errors + " </b>"	;
					}

					if(isOutput){
						if(!hideOutputBox){
							terminalOutput.value = output;
						}
						else{
							// they're old enough for this now...
							runMessage += "<br><u><b>Output:</u></b><br><b>" + output + " </b>";
						}
					}



					print(document.getElementById("runningOutput"+ uniqueID), runMessage);

					document.getElementById("runButton"+ uniqueID).disabled = false;
		       	},
		     	
		     	error: function(){
					print(document.getElementById("runningOutput"+ uniqueID), "Uh oh! Something went wrong with the website. Please email Sam about this." + "<br>");
					document.getElementById("runButton"+ uniqueID).disabled = false; 
		     	}
			});
		}

		compile.onclick = function(){
			compile.disabled = true;
			run.disabled = true;
			terminalInput.disabled = true;
			print(compilerTerminal, "<hr>", false);
			if (!editor.session.getUndoManager().isClean()){
				
				print(compilerTerminal, "<b>Please save your code before attempting to compile... </b><br>");
			} else {
				
				print(compilerTerminal, "Compiling code, please wait... <br>");

				compileCode(getValue(uniqueID));
			}
			
		}

		let isTypeMatch = function(variable, type){

			if (type == "int"){
				if (!isNaN(variable) && variable.indexOf(".")==-1 && variable.length <= 9){
					return true;
				}
			}

			if (type == "double"){
				if (!isNaN(variable)){
					return true;
				}
			}

			if (type == "boolean"){
				if (variable == "true" || variable == "false"){
					return true;
				}
			}

			if (type=="String"){
				return true
			}
		}

		run.onclick = function(){
			print(runningTerminal, "<hr>", false);
			let runMessage = "";
				
			run.disabled = true;
			terminalInput.disabled = true;
			print(runningTerminal, "Running code, please wait... <br>");

			let input = terminalInput.value;

			if (hideOutputBox){
				runCode(input);
			} else {
				let inputList = input.replace(/\s+/, " ").trim().split(" ");

				if (input.replace("/\s*/", "") == "" && inputNames.length > 0){
					print(runningTerminal, "<b>Your program needs an input! </b>");
				} else if (inputList.length == inputNames.length) {

					let passed = true;
					for (let i = 0; i < inputList.length; i++){
						if (!isTypeMatch(inputList[i], inputTypes[i])){
							passed = false;
						}
					}

					if (passed){
						runCode(input);
					} else{
						print(runningTerminal, "<b>Your input(s) had some type issues. Check the input and output types (located above the code editor box). </b>");
					}

				} else if (inputList.length < inputNames.length) {
					print(runningTerminal, "<b>You didn't enter enough inputs! </b>");
				} else if (inputList.length > inputNames.length) {
					print(runningTerminal, "<b>You entered too many inputs!</b>");
				} else{
					print(runningTerminal, "<b>Something went very wrong. Please reload the page and try again. </b>");
				}
			}
		
		run.disabled = false;
		terminalInput.disabled = false;

		};
	}
