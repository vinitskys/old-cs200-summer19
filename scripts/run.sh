cd ../workbooks/"$1"/Submissions/"$2"/"$3"

start=$SECONDS

rm -f run.out
rm -f run.log

shift # moves $X into $X-1
shift
shift

if timeout 60s java Main $* > run.out 2> run.log ; then
	echo "success"
else
	java Main $* > run.out 2> run.log 
fi

if [ $? -ne 124 ] 
then
	errorMessage=$(cat run.log)
else 
	errorMessage="Your program took too long to complete, and was terminated by the server."
	
fi

echo "%%OUTPUT%%$(cat run.out)"
echo "%%ERRORS%%$errorMessage"
