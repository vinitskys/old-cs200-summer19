<?php 

$javaCode = $_GET['program'];
$userID = $_GET['userID'];
$workbookID = $_GET['workbookID'];
$editorID = $_GET['editorID'];

if (!file_exists('../workbooks/'.$workbookID.'/Submissions/'.$userID)){
	mkdir('../workbooks/'.$workbookID.'/Submissions/'.$userID);
}

if (!file_exists('../workbooks/'.$workbookID.'/Submissions/'.$userID.'/'.$editorID)){
	mkdir('../workbooks/'.$workbookID.'/Submissions/'.$userID.'/'.$editorID);
}


file_put_contents('../workbooks/'.$workbookID.'/Submissions/'.$userID.'/'.$editorID."/Main.java", $javaCode);
$command = "./compile.sh " . "$workbookID "."$userID "."$editorID";

$output = shell_exec($command);
echo $output;

?>
