<?php 

$pageID = $_GET['pageID'];
$problemID = $_GET['problemID'];
$userID = $_GET['userID'];
$workbookID = $_GET['workbookID'];
$answer = $_GET['answer'];

if (!file_exists('../workbooks/'.$workbookID.'/Submissions/'.$userID)){
	mkdir('../workbooks/'.$workbookID.'/Submissions/'.$userID);
}

file_put_contents('../workbooks/'.$workbookID.'/Submissions/'.$userID."/submissionRecord.csv", 'page'.$pageID.",".$problemID.",".$answer."\n", FILE_APPEND);

?>
