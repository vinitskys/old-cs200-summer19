fs setacl ~/public/html/cs200-summer19 host:www read
fs setacl ~/public/html/cs200-summer19/pages host:www read
find ~/public/html/cs200-summer19/js -type d -exec fs sa {} host:www read \;
fs setacl ~/public/html/cs200-summer19/css host:www read 
fs setacl ~/public/html/cs200-summer19/scripts host:www read 
fs setacl ~/public/html/cs200-summer19/workbooks host:www read 

# set all the workbook stuff
for i in {1..16}
do
  fs setacl ~/public/html/cs200-summer19/workbooks/workbook$i host:www read 
  fs setacl ~/public/html/cs200-summer19/workbooks/workbook$i/Content host:www read 
  fs setacl ~/public/html/cs200-summer19/workbooks/workbook$i/Storage host:www write 
  fs setacl ~/public/html/cs200-summer19/workbooks/workbook$i/Submissions host:www write 
  fs setacl ~/public/html/cs200-summer19/workbooks/workbook$i/Statistics host:www write 
done

# set the workbook example
fs setacl ~/public/html/cs200-summer19/workbooks/workbookExample host:www read 
fs setacl ~/public/html/cs200-summer19/workbooks/workbookExample/Content host:www read 
fs setacl ~/public/html/cs200-summer19/workbooks/workbookExample/Storage host:www write 
fs setacl ~/public/html/cs200-summer19/workbooks/workbookExample/Submissions host:www write 
fs setacl ~/public/html/cs200-summer19/workbooks/workbookExample/Statistics host:www write 


# sandbox
fs setacl ~/public/html/cs200-summer19/workbooks/sandbox host:www read 
fs setacl ~/public/html/cs200-summer19/workbooks/sandbox/Content host:www read 
fs setacl ~/public/html/cs200-summer19/workbooks/sandbox/Submissions host:www write 
