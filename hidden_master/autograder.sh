# for each name in students.csv...

# get workbook # from cli
# cd to the proper folder

# were all of the expected questions answered?
# use expected.csv
# write score in the right line of grades.csv
# see which ones they got wrong, write those into stats.csv
# put grade report in feedback.csv

# check to see if writing prompt was there

# check correctness of any programs using test_casesX.csv 
# editor #X in is name/editorX/Main.java
# output scores to grades.csv
# put grade report in feedback.csv

# email grade breakdown + feedback to student
# calculate final grade, update massive MASTER_GRADES.csv 
